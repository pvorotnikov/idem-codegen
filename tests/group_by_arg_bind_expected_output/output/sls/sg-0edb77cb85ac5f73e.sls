rtb-0f840e96646e44c53:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-0f840e96646e44c53
    propagating_vgws: []
    resource_id: rtb-0f840e96646e44c53
    routes:
    - DestinationCidrBlock: 10.1.150.0/28
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    tags: []
    vpc_id: vpc-08f76fe175071e969


sg-0edb77cb85ac5f73e:
  aws.ec2.security_group.present:
  - resource_id: sg-0edb77cb85ac5f73e
  - name: default
  - vpc_id: vpc-08f76fe175071e969
  - description: default VPC security group


sgr-0470200c0811d69df:
  aws.ec2.security_group_rule.present:
  - name: sgr-0470200c0811d69df
  - resource_id: sgr-0470200c0811d69df
  - group_id: sg-0edb77cb85ac5f73e
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-0edb77cb85ac5f73e
      UserId: '123456789012'


sgr-06de8bb9a233fe279:
  aws.ec2.security_group_rule.present:
  - name: sgr-06de8bb9a233fe279
  - resource_id: sgr-06de8bb9a233fe279
  - group_id: sg-0edb77cb85ac5f73e
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


vpc-08f76fe175071e969:
  aws.ec2.vpc.present:
  - name: vpc-08f76fe175071e969
  - resource_id: vpc-08f76fe175071e969
  - instance_tenancy: default
  - tags:
    - Key: Name3
      Value: vijay-vpc-test3
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-054ac0d8549bbd88b
      CidrBlock: 10.1.150.0/28
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: false
  - enable_dns_support: true
