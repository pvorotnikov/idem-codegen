igw-89798ae0:
  aws.ec2.internet_gateway.present:
  - attachments:
    - State: available
      VpcId: vpc-dcae57b5
    name: igw-89798ae0
    resource_id: igw-89798ae0
    vpc_id:
    - vpc-dcae57b5


rtb-6bcc0d02:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-6bcc0d02
    propagating_vgws: []
    resource_id: rtb-6bcc0d02
    routes:
    - DestinationCidrBlock: 172.31.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-89798ae0
      Origin: CreateRoute
      State: active
    tags:
    - Key: Name
      Value: idem-test
    vpc_id: vpc-dcae57b5


sg-01a41f68:
  aws.ec2.security_group.present:
  - resource_id: sg-01a41f68
  - name: rds-launch-wizard-2
  - vpc_id: vpc-dcae57b5
  - tags:
    - Key: AutomaticCleanExpirationTime
      Value: '2020-07-10T09:57:33.882Z'
  - description: 'Created from the RDS Management Console: 2018/01/28 19:09:27'


sg-0fc412cebfb987038:
  aws.ec2.security_group.present:
  - resource_id: sg-0fc412cebfb987038
  - name: photon-model-sg
  - vpc_id: vpc-dcae57b5
  - description: abc Photon model security group


sg-d68355bf:
  aws.ec2.security_group.present:
  - resource_id: sg-d68355bf
  - name: default
  - vpc_id: vpc-dcae57b5
  - description: default VPC security group


sgr-01f124742859acf30:
  aws.ec2.security_group_rule.present:
  - name: sgr-01f124742859acf30
  - resource_id: sgr-01f124742859acf30
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 8080
  - to_port: 8080
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-02dcf5ca5bb9218b9:
  aws.ec2.security_group_rule.present:
  - name: sgr-02dcf5ca5bb9218b9
  - resource_id: sgr-02dcf5ca5bb9218b9
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 1
  - to_port: 65535
  - cidr_ipv4: 172.31.0.0/16
  - tags: []


sgr-04299d060ac3ebe2a:
  aws.ec2.security_group_rule.present:
  - name: sgr-04299d060ac3ebe2a
  - resource_id: sgr-04299d060ac3ebe2a
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 2376
  - to_port: 2376
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-05e5771dece9c1ed9:
  aws.ec2.security_group_rule.present:
  - name: sgr-05e5771dece9c1ed9
  - resource_id: sgr-05e5771dece9c1ed9
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-05e6ceaaebacf5dc1:
  aws.ec2.security_group_rule.present:
  - name: sgr-05e6ceaaebacf5dc1
  - resource_id: sgr-05e6ceaaebacf5dc1
  - group_id: sg-d68355bf
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-d68355bf
      UserId: '123456789012'


sgr-08163ca9869d9fce3:
  aws.ec2.security_group_rule.present:
  - name: sgr-08163ca9869d9fce3
  - resource_id: sgr-08163ca9869d9fce3
  - group_id: sg-01a41f68
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 5432
  - to_port: 5432
  - cidr_ipv4: 24.7.88.198/32
  - tags: []


sgr-09a323e0c1f512b46:
  aws.ec2.security_group_rule.present:
  - name: sgr-09a323e0c1f512b46
  - resource_id: sgr-09a323e0c1f512b46
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 22
  - to_port: 22
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a1c13bf84aef05b3:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a1c13bf84aef05b3
  - resource_id: sgr-0a1c13bf84aef05b3
  - group_id: sg-0fc412cebfb987038
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a35370111c0c05fe:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a35370111c0c05fe
  - resource_id: sgr-0a35370111c0c05fe
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 2375
  - to_port: 2375
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0ca3ad84b6ac0506e:
  aws.ec2.security_group_rule.present:
  - name: sgr-0ca3ad84b6ac0506e
  - resource_id: sgr-0ca3ad84b6ac0506e
  - group_id: sg-01a41f68
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0e211bb288c7a0d96:
  aws.ec2.security_group_rule.present:
  - name: sgr-0e211bb288c7a0d96
  - resource_id: sgr-0e211bb288c7a0d96
  - group_id: sg-d68355bf
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0eb7727a9d7a24f7d:
  aws.ec2.security_group_rule.present:
  - name: sgr-0eb7727a9d7a24f7d
  - resource_id: sgr-0eb7727a9d7a24f7d
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 80
  - to_port: 80
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


subnet-5f1c0227:
  aws.ec2.subnet.present:
  - name: subnet-5f1c0227
  - resource_id: subnet-5f1c0227
  - vpc_id: vpc-dcae57b5
  - cidr_block: 172.31.16.0/20
  - availability_zone: eu-west-3b
  - tags:
    - Key: AutomaticCleanExpirationTime
      Value: '2020-07-10T09:57:33.882Z'


vpc-dcae57b5:
  aws.ec2.vpc.present:
  - name: vpc-dcae57b5
  - resource_id: vpc-dcae57b5
  - instance_tenancy: default
  - tags:
    - Key: Name
      Value: default
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-5e854037
      CidrBlock: 172.31.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: true
  - enable_dns_support: true
