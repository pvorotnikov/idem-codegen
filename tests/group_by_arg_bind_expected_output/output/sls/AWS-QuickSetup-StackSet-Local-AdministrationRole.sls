AWS-QuickSetup-StackSet-Local-AdministrationRole:
  aws.iam.role.present:
  - resource_id: AWS-QuickSetup-StackSet-Local-AdministrationRole
  - name: AWS-QuickSetup-StackSet-Local-AdministrationRole
  - arn: arn:aws:iam::123456789012:role/AWS-QuickSetup-StackSet-Local-AdministrationRole
  - id: AROAX2FJ77DCYHZJS2UUV
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "cloudformation.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWS-QuickSetup-StackSet-Local-AdministrationRole-AssumeRole-AWS-QuickSetup-StackSet-Local-ExecutionRole:
  aws.iam.role_policy.present:
  - resource_id: AWS-QuickSetup-StackSet-Local-AdministrationRole-AssumeRole-AWS-QuickSetup-StackSet-Local-ExecutionRole
  - role_name: AWS-QuickSetup-StackSet-Local-AdministrationRole
  - name: AssumeRole-AWS-QuickSetup-StackSet-Local-ExecutionRole
  - policy_document: '{"Statement": [{"Action": ["sts:AssumeRole"], "Effect": "Allow",
      "Resource": ["arn:*:iam::*:role/AWS-QuickSetup-StackSet-Local-ExecutionRole"]}],
      "Version": "2012-10-17"}'
