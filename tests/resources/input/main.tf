module "iam" {
  source      = "iam"
  region      = var.region
  profile     = var.profile
  owner       = var.owner
  clusterName = var.clusterName
  admin_users = var.admin_users
  cogs        = var.cogs
  singleAz    = var.singleAz
}


module "cluster" {
  source                                = "cluster"
  region                                = var.region
  profile                               = var.profile
  owner                                 = var.owner
  clusterName                           = var.clusterName
  clusterVersion                        = var.clusterVersion
  VpcSuperNet                           = var.VpcSuperNet
  create_vpc                            = var.create_vpc
  create_subnets                        = var.create_subnets
  pvt_subnet_name                       = var.pvt_subnet_name
  public_subnet_name                    = var.public_subnet_name
  VpcName                               = var.VpcName
  cluster_pvt_subnet_cidr               = var.cluster_pvt_subnet_cidr
  cluster_public_subnet_cidr            = var.cluster_public_subnet_cidr
  aws_iam_role-xyz-admin                = module.iam.aws_iam_role-xyz-admin
  aws_kms_key-credstash_key-arn         = module.iam.aws_kms_key-credstash_key-arn
  aws_kms_key-credstash_key-key-id      = module.iam.aws_kms_key-credstash_key-key-id
  admin_users                           = var.admin_users
  cluster_admin                         = var.admin_users
  cluster_edit                          = var.cluster_edit
  cluster_read                          = var.cluster_read
  aws_iam_role-cluster-node-arn         = module.iam.aws_iam_role-cluster-node-arn
  cogs                                  = var.cogs
  transit-gw                            = var.transit-gw
  enable-jenkins-rolling-upgrade-policy = var.enable-jenkins-rolling-upgrade-policy
  singleAz                              = var.singleAz
  cross_account_beachops_domain_profile = var.cross_account_beachops_domain_profile
  msk_kafka_domain                      = var.msk_kafka_domain
}

module "ccc-ensemble-resources" {
  source      = "ccc-ensemble-resources"
  service     = "common"
  owner       = var.owner
  clusterName = var.clusterName
  product     = "ensemble"
  profile     = var.profile
  region      = var.region
}

temp {
  backend "s3" {
  }
}
