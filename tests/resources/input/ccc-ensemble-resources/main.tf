# Bucket for data-solr core backups
resource "aws_s3_bucket" "ccc" {
    bucket = "ccc-${var.clusterName}"
    acl = var.acl

    server_side_encryption_configuration {
      rule {
        apply_server_side_encryption_by_default {
            sse_algorithm     = "AES256"
        }
      }
    }

    tags = merge(
    local.tags,
    {
      "Service"       = "ccc"
      "Precious"      = "true"
    }
  )
     lifecycle_rule {
      id      = "Delete"
      enabled = true

      tags = {
       Tmp    = "true"
      }

      expiration {
        days = 30
      }
     }
}

# Policy for attaching to iam user for the region
module "log-rca-policy" {
  source      = "../../iam/modules/policy/"
  profile     = var.profile
  name_prefix = "ccc-s3-eks-${var.clusterName}-policy"
  product     = var.product
  service     = var.service
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::ccc-${var.clusterName}",
                "arn:aws:s3:::ccc-${var.clusterName}/*"
            ]
        }
    ]
}
EOF

}
