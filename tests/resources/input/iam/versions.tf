
temp {
  required_version = ">= 0.14"
  required_providers {
    aws = {
      source = "train/aws"
      version = "3.74.1"
    }
  }
}
