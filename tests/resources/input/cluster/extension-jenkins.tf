resource "aws_iam_user" "extension-jenkins" {
  name = "extension-jenkins-${var.clusterName}"
  path = "/xyz/${var.clusterName}/"
  tags = local.tags
}

output "aws_iam_user-extension-jenkins-name" {
  value = aws_iam_user.extension-jenkins.name
}

resource "aws_iam_user_policy" "extension-jenkins" {
  name = "extension-jenkins-${var.clusterName}"
  user = aws_iam_user.extension-jenkins.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "xyz:DescribeCluster"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:xyz:${var.region}:${data.aws_caller_identity.current.account_id}:cluster/${var.clusterName}"
    },
    {
        "Sid": "",
        "Effect": "Allow",
        "Action": [
            "sts:AssumeRole"
        ],
        "Resource": [
            "${aws_iam_role.xyz-jenkins.arn}"
        ]
    },
    {
      "Sid": "",
      "Action": "s3:*",
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::ssm-ansible-${var.profile}",
        "arn:aws:s3:::ssm-ansible-${var.profile}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
          "ssm:StartSession"
      ],
      "Resource": "*",
      "Condition": {
        "StringLike": {
          "ssm:resourceTag/KubernetesCluster": [
            "${var.clusterName}"
          ]
        }
      }
    },
    {
      "Effect": "Allow",
      "Action": [
        "ssm:TerminateSession"
      ],
      "Resource": "*"
    }
  ]
}
EOF

}

# This policy should only be applied when the cluster needs a rolling update of the ASGs via jenkins, can be contolled via a variable
resource "aws_iam_user_policy" "extension-jenkins-rolling-upgrade" {
  count = var.enable-jenkins-rolling-upgrade-policy ? 1 : 0
  name  = "extension-jenkins-rolling-upgrade-${var.clusterName}"
  user  = aws_iam_user.extension-jenkins.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
            "Effect": "Allow",
            "Action": "ec2:Describe*",
            "Resource": "*"
    },
    {
        "Sid": "",
        "Effect": "Allow",
        "Action": [
            "sts:AssumeRole"
        ],
        "Resource": [
            "${aws_iam_role.xyz-jenkins.arn}"
        ]
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": [
        "autoscaling:DeleteTags",
        "autoscaling:ResumeProcesses",
        "autoscaling:CreateOrUpdateTags",
        "autoscaling:UpdateAutoScalingGroup",
        "autoscaling:SuspendProcesses",
        "autoscaling:TerminateInstanceInAutoScalingGroup"
      ],
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "autoscaling:ResourceTag/KubernetesCluster": "${var.clusterName}"
        }
      }
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": [
                "xyz:UpdateClusterVersion",
                "ec2:DescribeInstances",
                "ec2:RebootInstances",
                "autoscaling:DescribeAutoScalingGroups",
                "xyz:DescribeUpdate",
                "xyz:DescribeCluster",
                "xyz:ListClusters",
                "xyz:CreateCluster"
      ],
      "Resource": "*"
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": "ec2:DescribeInstances",
      "Resource": "*"
    }
  ]
}
EOF

}

##### Jenkins service account for pipeline
## IAM read role for describing the cluster
resource "aws_iam_role" "xyz-jenkins" {
  name = "xyz-${var.clusterName}-jenkins"
  tags = local.tags

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${data.aws_caller_identity.current.account_id}"
      },
      "Action": "sts:AssumeRole",
      "Condition": {
        "ForAnyValue:StringEquals": {
          "aws:username": ${jsonencode(var.admin_users)}
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_iam_user.extension-jenkins.arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

}

#          "aws:username": ${jsonencode(distinct(flatten(list(var.default_admins, var.admin_users))))}

output "aws_iam_role-xyz-jenkins-arn" {
  value = aws_iam_role.xyz-jenkins.arn
}

## IAM role policy for xyz-jenkins
resource "aws_iam_policy" "xyz-jenkins" {
  name        = "xyz-${var.clusterName}-jenkins"
  description = "iam policy for xyz-jenkins"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "xyz:DescribeCluster",
      "Effect": "Allow",
      "Resource": "arn:aws:xyz:${var.region}:${data.aws_caller_identity.current.account_id}:cluster/${var.clusterName}"
    }
  ]
}
EOF

}

output "aws_iam_policy-xyz-jenkins-id" {
  value = aws_iam_policy.xyz-jenkins.id
}

## iam role policy attachment for xyz-jenkins
resource "aws_iam_role_policy_attachment" "xyz-jenkins" {
  role       = aws_iam_role.xyz-jenkins.name
  policy_arn = aws_iam_policy.xyz-jenkins.arn
}
