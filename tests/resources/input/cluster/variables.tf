variable "clusterName" {
}

variable "clusterVersion" {
}

variable "region" {
}

variable "owner" {
}

variable "profile" {
}

variable "VpcSuperNet" {
}

variable "create_vpc"{
}

variable "create_subnets" {
}

variable "VpcName" {
}

variable "pvt_subnet_name" {
}

variable "public_subnet_name"{
}

variable "cluster_pvt_subnet_cidr" {
  type    = list(string)
  default = [""]
}

variable "cluster_public_subnet_cidr" {
  type    = list(string)
  default = [""]
}

variable "aws_iam_role-xyz-admin" {
}

variable "aws_kms_key-credstash_key-arn" {
}

variable "aws_kms_key-credstash_key-key-id" {
}

variable "admin_users" {
  type    = list(string)
  default = [""]
}

variable "cluster_admin" {
  type    = list(string)
  default = [""]
}

variable "cluster_edit" {
  type    = list(string)
  default = [""]
}

variable "cluster_read" {
  type    = list(string)
  default = [""]
}

variable "aws_iam_role-cluster-node-arn" {
}

variable "cogs" {
}

variable "automation" {
  default = true
}

variable "bastion_security_group_cidr" {
  type = map(string)

  default = {
    sym-dev4_us-west-2         = "172.16.0.0/16" # sym-dev4-mgmt-2
    sym-staging_ap-northeast-1 = "172.25.0.0/16" # staging-jp-mgmt-2
    # sym-staging_eu-west-2 = "172.29.0.0/16" # staging-uk-mgmt-2  => THIS is old
    sym-staging_eu-west-2   = "172.22.0.0/16" # staging-uk-mgmt-2
    sym-staging_us-west-2   = "172.22.0.0/16" # staging-us-mgmt-2
    sym-prod_ap-southeast-2 = "10.223.0.0/16" # prod-au-mgmt-2
    sym-prod_us-west-2      = "10.223.0.0/16" # prod-us-mgmt-2
    sym-prod_ap-southeast-1 = "10.223.0.0/16" # prod-us-mgmt-2
    sym-prod_eu-central-1   = "10.223.0.0/16" # prod-us-mgmt-2
    # sym-prod_ap-northeast-1 = "172.27.0.0/16" # prod-jp-mgmt-2 ==> This is old
    sym-prod_ap-northeast-1 = "10.223.0.0/16" # prod-jp-mgmt-2
    # sym-prod_eu-west-2 = "172.31.0.0/16" # prod-uk-mgmt-2 => This is old
    sym-prod_ca-central-1 = "10.223.0.0/16" # prod-ca-mgmt-2
    sym-prod_eu-west-2    = "10.223.0.0/16" # prod-us-mgmt-2
    mango_us-west-2     = "172.22.0.0/16" # mango-us-mgmt-2
    scaleperf_us-west-2   = "172.22.0.0/16" # scaleperf-us-mgmt-2
    sym-prod_sa-east-1    = "10.223.0.0/16" # prod-br-1
  }
}

variable "transit-gw" {
  default = 0
}

variable "enable-jenkins-rolling-upgrade-policy" {
  default = false
}

variable "singleAz" {
  default = false
}

variable "cross_account_beachops_domain_profile" {
  default = ""
}

# This variable if true associates this xyz VPC with corresponding kafka.{regin}.amazonaws.com private route53 domain
# This is setup so that we can use the same CNAME as MSK while using other means of connectivity such as
# VPC peering or private link. Zone must exist in account resources of this account in which the cluster is present
# {region} is region of the xyz cluster.
variable "msk_kafka_domain" {
  default = false
}
