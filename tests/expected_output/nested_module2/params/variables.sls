include:
- common-variables
local_bastion_security_group_cidr_all: '${contains(keys(var.bastion_security_group_cidr),"${var.profile}_${var.region}")
  ? var.bastion_security_group_cidr : merge(var.bastion_security_group_cidr,{''${var.profile}_${var.region}'':
  ''${var.VpcSuperNet}0.0/16''})}'
