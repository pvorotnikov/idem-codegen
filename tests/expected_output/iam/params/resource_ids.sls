aws_iam_policy.xyz-admin: arn:aws:iam::123456789012:policy/xyz-idem-test-admin
aws_iam_role.cluster-node: idem-test-temp-xyz-cluster-node
aws_iam_role.xyz-admin: xyz-idem-test-admin
aws_iam_role_policy.Amazon_EBS_CSI_Driver: idem-test-temp-xyz-cluster-node-Amazon_EBS_CSI_Driver
aws_iam_role_policy.credstash_access_policy: idem-test-temp-xyz-cluster-node-credstash_xyz_idem-test_access_policy
