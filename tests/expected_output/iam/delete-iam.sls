aws_iam_policy.xyz-admin:
  aws.iam.policy.absent:
  - name: xyz-idem-test-admin
aws_iam_role.cluster-node:
  aws.iam.role.absent:
  - name: idem-test-temp-xyz-cluster-node
aws_iam_role.xyz-admin:
  aws.iam.role.absent:
  - name: xyz-idem-test-admin
aws_iam_role_policy.Amazon_EBS_CSI_Driver:
  aws.iam.role_policy.absent:
  - role_name: idem-test-temp-xyz-cluster-node
  - name: Amazon_EBS_CSI_Driver
aws_iam_role_policy.credstash_access_policy:
  aws.iam.role_policy.absent:
  - role_name: idem-test-temp-xyz-cluster-node
  - name: credstash_xyz_idem-test_access_policy
aws_iam_role_policy_attachment.cluster-node-AmazonEC2ContainerRegistryReadOnly:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly
aws_iam_role_policy_attachment.cluster-node-AmazonEC2ReadOnlyAccess:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess
aws_iam_role_policy_attachment.cluster-node-AmazonEC2RoleforSSM:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM
aws_iam_role_policy_attachment.cluster-node-AmazonElasticFileSystemFullAccess:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess
aws_iam_role_policy_attachment.cluster-node-AmazonxyzWorkerNodePolicy:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy
aws_iam_role_policy_attachment.cluster-node-Amazonxyz_CNI_Policy:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy
aws_iam_role_policy_attachment.xyz-admin:
  aws.iam.role_policy_attachment.absent:
  - role_name: xyz-idem-test-admin
  - policy_arn: arn:aws:iam::123456789012:policy/xyz-idem-test-admin
