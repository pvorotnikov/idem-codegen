VpcName: ''
VpcSuperNet: 10.170.
aws_iam_role-cluster-node-arn: ''
aws_iam_role-xyz-admin: ''
aws_kms_key-credstash_key-arn: ''
aws_kms_key-credstash_key-key-id: ''
bastion_security_group_cidr:
  mango_us-west-2: 172.22.0.0/16
  scaleperf_us-west-2: 172.22.0.0/16
  sym-dev4_us-west-2: 172.16.0.0/16
  sym-prod_ap-northeast-1: 10.223.0.0/16
  sym-prod_ap-southeast-1: 10.223.0.0/16
  sym-prod_ap-southeast-2: 10.223.0.0/16
  sym-prod_ca-central-1: 10.223.0.0/16
  sym-prod_eu-central-1: 10.223.0.0/16
  sym-prod_eu-west-2: 10.223.0.0/16
  sym-prod_sa-east-1: 10.223.0.0/16
  sym-prod_us-west-2: 10.223.0.0/16
  sym-staging_ap-northeast-1: 172.25.0.0/16
  sym-staging_eu-west-2: 172.22.0.0/16
  sym-staging_us-west-2: 172.22.0.0/16
clusterVersion: '1.20'
cluster_admin:
- user1
cluster_edit:
- user1
cluster_public_subnet_cidr: '[""]'
cluster_pvt_subnet_cidr: '[""]'
cluster_read:
- user1
create_subnets: ''
create_vpc: ''
cross_account_beachops_domain_profile: ''
enable-jenkins-rolling-upgrade-policy: 'True'
include:
- common-variables
msk_kafka_domain: 'True'
public_subnet_name: ''
pvt_subnet_name: ''
transit-gw: 0
