aws_iam_role.cluster:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.cluster")}}
  - name: {{ params["clusterName"] }}-temp-xyz-cluster
  - max_session_duration: 3600
  - tags: {{ params["local_tags"] }}
  - assume_role_policy_document:
      {
          "Statement": [
              {
                  "Action": "sts:AssumeRole",
                  "Effect": "Allow",
                  "Principal": {
                      "Service": "xyz.amazonaws.com"
                  }
              }
          ],
          "Version": "2012-10-17"
      }

aws_iam_role_policy_attachment.cluster-AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy

aws_iam_role_policy_attachment.cluster-AmazonxyzServicePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzServicePolicy
