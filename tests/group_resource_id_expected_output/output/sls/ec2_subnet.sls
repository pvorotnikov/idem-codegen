subnet-0094b72dfb7ce6131:
  aws.ec2.subnet.present:
  - name: subnet-0094b72dfb7ce6131
  - resource_id: subnet-0094b72dfb7ce6131
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.208.0/20
  - availability_zone: eu-west-3b
  - tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-control-public


subnet-039e53122e038d38c:
  aws.ec2.subnet.present:
  - name: subnet-039e53122e038d38c
  - resource_id: subnet-039e53122e038d38c
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.128.0/18
  - availability_zone: eu-west-3c
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz-node-private
    - Key: Owner
      Value: org1


subnet-050732fa4616470d9:
  aws.ec2.subnet.present:
  - name: subnet-050732fa4616470d9
  - resource_id: subnet-050732fa4616470d9
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.0.0/18
  - availability_zone: eu-west-3a
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz-node-private
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: Owner
      Value: org1


subnet-05a0fbce3b40dcd05:
  aws.ec2.subnet.present:
  - name: subnet-05a0fbce3b40dcd05
  - resource_id: subnet-05a0fbce3b40dcd05
  - vpc_id: vpc-0dbee6437661777d4
  - cidr_block: 10.0.107.0/24
  - availability_zone: eu-west-3b
  - tags:
    - Key: Name
      Value: aws_ec2_subnet_for_xyz


subnet-05dfaa0d01a337199:
  aws.ec2.subnet.present:
  - name: subnet-05dfaa0d01a337199
  - resource_id: subnet-05dfaa0d01a337199
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.64.0/18
  - availability_zone: eu-west-3b
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: COGS
      Value: OPEX
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-temp-xyz-node-private


subnet-085c807f9dcd3bccd:
  aws.ec2.subnet.present:
  - name: subnet-085c807f9dcd3bccd
  - resource_id: subnet-085c807f9dcd3bccd
  - vpc_id: vpc-0dbee6437661777d4
  - cidr_block: 10.0.78.0/24
  - availability_zone: eu-west-3a
  - tags:
    - Key: Name
      Value: aws_ec2_subnet_for_xyz_worker


subnet-09cecc8c853637d3b:
  aws.ec2.subnet.present:
  - name: subnet-09cecc8c853637d3b
  - resource_id: subnet-09cecc8c853637d3b
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.192.0/20
  - availability_zone: eu-west-3a
  - tags:
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: Name
      Value: idem-test-temp-xyz-control-public
    - Key: Environment
      Value: test-dev
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1


subnet-0d68d61b1ab708d42:
  aws.ec2.subnet.present:
  - name: subnet-0d68d61b1ab708d42
  - resource_id: subnet-0d68d61b1ab708d42
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.224.0/20
  - availability_zone: eu-west-3c
  - tags:
    - Key: Owner
      Value: org1
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-control-public
    - Key: kubernetes.io/cluster/idem-test
      Value: shared


subnet-5f1c0227:
  aws.ec2.subnet.present:
  - name: subnet-5f1c0227
  - resource_id: subnet-5f1c0227
  - vpc_id: vpc-dcae57b5
  - cidr_block: 172.31.16.0/20
  - availability_zone: eu-west-3b
  - tags:
    - Key: AutomaticCleanExpirationTime
      Value: '2020-07-10T09:57:33.882Z'
